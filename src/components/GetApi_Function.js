import React, { useEffect, useLayoutEffect, useState } from 'react'

function GetApi() {
    const[data,setData]=useState([])


   // componenetDidmount in class component
    useEffect(()=>{
        fetch('https://jsonplaceholder.typicode.com/posts').then((result)=>{
        result.json().then((response)=>{
            // console.log(response)
            setData(response)
        })
    })
    },[])
    console.log(data)
    

  return (
    <div>
        <table>
        {
      data.map((item)=>
        
            <tr>
                <td>{item.userId}</td>
                <td>{item.id}</td>
                <td>{item.title}</td>
            </tr>
      )
    }
        </table>
      
      
    </div>
  )
}

export default GetApi

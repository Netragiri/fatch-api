import axios from 'axios'
import React, { Component } from 'react'

 class AxiosGet extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         values:[],
         error:""
      }
    }
    componentDidMount(){
        axios.get('http://localhost:3003/contacts')
        .then(response=>{
            console.log(response)
            this.setState({values:response.data})
        })
        .catch(error=>
            {
                console.log(error)
                this.setState({error:`can't load api`})
            }
            )
    }
  render() {
      const {values,error}=this.state
    return (
      <div>

        {values.map(item=>
        <div key={item.id}>
            <p>{item.id},{item.title}</p>
        </div>
        )}
        {<div>{error}</div>}
      </div>
    )
  }
}

export default AxiosGet


import React, { Component } from 'react'

export class GetApi_Class extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         value:[]
      }
    }
    componentDidMount() {

                 fetch('https://jsonplaceholder.typicode.com/posts')
                 .then(response => response.json())
                 .then(data =>{
                    this.setState({ value: data })
                 } );
    }
  render() { 
    return (
      <div>
      
                <p>values: 
                    
                    {this.state.value.map(item=><div key={item.id}>

                    {item.id},{item.title},{item.userId}

                </div>
                )}</p>
      
      </div>
    )
  }
}

export default GetApi_Class

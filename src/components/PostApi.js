import './App.css';
import React, { useEffect, useState } from 'react'
function App() {
 const [userId,setUserId]=useState("");
 const [title,setTitle]=useState("");
 const [body,setBody]=useState("");
function saveData()
{
  let data={userId,title,body}
// console.warn(data);
  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body:JSON.stringify(data)
  }).then((resp)=>{
    resp.json().then((result)=>{
      console.warn("result",result)
    })
  })
}
  return (
    <div className="App">
      <h1>POST API Example </h1>  
      <input type="text" name="userId" value={userId} onChange={(e)=>{setUserId(e.target.value)}}  /> <br /> <br />
      <input type="text" name="title"  value={title} onChange={(e)=>{setTitle(e.target.value)}} /> <br /> <br />
      <input type="text" name="body"  value={body} onChange={(e)=>{setBody(e.target.value)}}/> <br /> <br />
      <button type="button" onClick={saveData} >Save New User</button>
    </div>
  );
}
export default App;
import React, { Component } from 'react'
import axios from 'axios'

class AxiosPost extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         userId:'',
         title:'',
         body:''

      }
    }
    changehandler=(event)=>{
        event.preventDefault()
        this.setState({[event.target.name]:event.target.value})
    }

    submithandler=(event)=>{
        event.preventDefault()
        console.log(this.state)
        axios.post('http://localhost:3003/contacts',this.state)
        .then(response=>{
            console.log(response)
        })
        .catch(error=>{
            console.log(error)
        })
    }
    
  render() {
    return (
        <>
        <form onSubmit={this.submithandler}>

            <div>
                Userid:
                <input type="text" name="userId" value={this.state.userId}
                 onChange={this.changehandler}></input><br /><br />

                Title:
                <input type="text" name="title" value={this.state.title} 
                 onChange={this.changehandler}></input><br /><br />

                Body:
                 <input type="text" name="body" value={this.state.body}
                   onChange={this.changehandler}></input><br /><br />

                 <button type='submit'>Submit</button>

            </div>

        </form>
      </>
    )
  }
}

export default AxiosPost
